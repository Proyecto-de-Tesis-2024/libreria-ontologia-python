from owlready2 import get_ontology
from owlready2 import *
# Especifica la ruta de tu archivo de ontología
ruta_ontologia = "C:\\Users\\MVBU HUAWEI\\Desktop\\ontology-university.owx"

# Carga la ontología desde el archivo
ontologia = get_ontology("file://" + ruta_ontologia).load()

# Accede a la clase Materia
clase_materia = ontologia.Materia

# Imprime los valores de las Data Properties de las instancias de la clase Materia
for instancia_materia in clase_materia.instances():
    print("Instancia de Materia:", instancia_materia)
    print("Nombre:", instancia_materia.tieneNombreMateria if hasattr(instancia_materia, "tieneNombreMateria") else "No definido")
    print("OtraPropiedad:", instancia_materia.tieneClave if hasattr(instancia_materia, "otraPropiedad") else "No definido")
    # Agrega más propiedades según las que tengas definidas en tu ontología
    print("------------------------")








    


