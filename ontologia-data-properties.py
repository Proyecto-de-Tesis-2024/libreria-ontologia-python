from owlready2 import get_ontology
from owlready2 import *
# Especifica la ruta de tu archivo de ontología
ruta_ontologia = "C:\\Users\\MVBU HUAWEI\\Desktop\\ontology-university.owx"

# Carga la ontología desde el archivo
onto = get_ontology("file://" + ruta_ontologia).load()

# Accede a una clase específica
clase_materia = onto.Materia
print(onto.data_properties())

# Obtener propiedades de datos de la clase Materia
materia_class = onto.Materia
print("Propiedades de datos para la clase Materia:")
for dp in onto.data_properties():
    if dp.domain[0] == materia_class:
        print(f"Nombre: {dp.name}, IRI: {dp.iri}")

