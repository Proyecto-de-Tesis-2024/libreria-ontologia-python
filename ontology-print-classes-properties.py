#Utilizando libreria owlready2 para ontologias
from owlready2 import get_ontology
from owlready2 import *
# Especifica la ruta de tu archivo de ontología
ruta_ontologia = "C:\\Users\\MVBU HUAWEI\\Desktop\\ontology-university.owx"

# Carga la ontología desde el archivo
ontologia = get_ontology("file://" + ruta_ontologia).load()

#print(dir(ontologia))

#clases de la Ontologia Universidad
print("Clases en la ontología:")
for clase in ontologia.classes():
    print(clase.name)

# Ejemplo: Imprimir todas las propiedades de la ontologia Universidad
print("\nPropiedades en la ontología:")
for propiedad in ontologia.properties():
    print(propiedad.name)
