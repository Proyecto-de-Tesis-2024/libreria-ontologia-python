from owlready2 import get_ontology
from owlready2 import *
# Especifica la ruta de tu archivo de ontología
ruta_ontologia = "C:\\Users\\MVBU HUAWEI\\Desktop\\ontology-university.owx"

# Carga la ontología desde el archivo
onto = get_ontology("file://" + ruta_ontologia).load()

# Obtén la clase Materia
materia_class = onto.Materia
print(materia_class.get_class_properties())

# Lista las propiedades de objeto de la clase Materia
# Falta identificar la relaciones de donde a donde van.
print("Object Properties para la clase Materia:")
for prop in onto.object_properties():
    print(prop.domain[0])
    if prop.domain[0] == materia_class:
        print(f"Nombre: {prop.name}, IRI: {prop.iri}")
        